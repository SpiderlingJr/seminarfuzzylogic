package dvr;

public class Triangular extends FuzzyFunction {
    private double dMiddle; //peak coordinate

    public Triangular(double dLeft, double peak, double dRight, String memshipName){
        this.dLeft = dLeft;
        this.dMiddle = peak;
        this.dRight = dRight;
        this.memshipName = memshipName;
        this.functionType = 'd';
    }

    public double getMemshipValue(double x) {

        if (x < dLeft || x > dRight) {return 0;}
        else if (x < dMiddle) {
            return (x - dLeft )/(dMiddle - dLeft); //Classic upper Linear Memship Function
        }
        else if (x == dMiddle) {return 1.0;} // Peak Value
        else if (x < dRight) {
            return (dRight - x)/(dRight - dMiddle); //Classic downer Linear Memship Function
        }
        else return 0;
    }
    public double getCentroid() { return (dLeft+dMiddle+dRight)/3; }
}
