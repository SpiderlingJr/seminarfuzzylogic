package dvr;

public abstract class FuzzyFunction {

    protected double dLeft, dRight; //Boundaries of Membership > 0
    protected String memshipName;
    protected char functionType; //triangular or

    public FuzzyFunction(){

    }
    abstract public double getMemshipValue(double x);
    abstract public double getCentroid();
}
