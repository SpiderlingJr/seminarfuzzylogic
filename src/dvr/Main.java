package dvr;

import java.util.Arrays;

/**
 * Code auf C++ Basis modifiziert entnommen von
 * https://www.thegeekstuff.com/2014/09/fuzzy-logic-cpp/
 */
public class Main {
    static String[] positionCategory = {"farLeft", "left", "middle", "right", "farRight"};
    public static void main(String[] args) {

        /* 1. Crisp Values */
        double positionInput =Math.random()*1024;
        double speedInput = Math.random()*100;

        System.out.println("Processed Values: ");
        System.out.println("Position Input: " + positionInput);
        System.out.println("Speed Input: " + speedInput);
        double[] fuzzyBreakResult = {0.0,0.0,0.0};
        double[] fuzzySpeed = new double[3];
        double[] fuzzyPosition = new double[5];


        // ArrayList<FuzzyFunction> FuzzySetFahrstreifen = new ArrayList<>();
        FuzzyFunction[] FuzzyBreakSet = new FuzzyFunction[3];
        FuzzyFunction[] FuzzyPositionSet = new FuzzyFunction[5];
        FuzzyFunction[] FuzzySpeedSet = new FuzzyFunction[3];
        Rule[] RuleSet = new Rule[15];


        FuzzyBreakSet[0] = new Triangular(0,0,60, "SoftBreak");
        FuzzyBreakSet[1] = new Trapezoid(10,50,60,90, "MediocreBreak");
        FuzzyBreakSet[2] = new Triangular(40, 100,100, "HardBreak");

        FuzzyPositionSet[0] = new Trapezoid(0,0, 64,192,"farLeft");
        FuzzyPositionSet[1] = new Trapezoid(64, 192,320,448, "Left");
        FuzzyPositionSet[2] = new Trapezoid(320, 448, 576, 704, "Middle");
        FuzzyPositionSet[3] = new Trapezoid(576, 704, 832, 960, "Right");
        FuzzyPositionSet[4] = new Trapezoid(832, 960, 1024, 1024, "farRight");

        FuzzySpeedSet[0] = new Triangular(0,0,45,  "Slow");
        FuzzySpeedSet[1] = new Trapezoid(0,45, 55, 100, "Moderate");
        FuzzySpeedSet[2] = new Triangular(55,100, 100, "Fast");

        /* 2. FUZZIFY + Ruleset Building */
        RuleSet[0] = new Rule(0,0, "MediocreBreak");
        RuleSet[1] = new Rule(0,1, "MediocreBreak");
        RuleSet[2] = new Rule(0,2, "HardBreak");
        RuleSet[3] = new Rule(1,0, "SoftBreak");
        RuleSet[4] = new Rule(1,1, "MediocreBreak");
        RuleSet[5] = new Rule(1,2, "MediocreBreak");
        RuleSet[6] = new Rule(2,0, "SoftBreak");
        RuleSet[7] = new Rule(2,1, "SoftBreak");
        RuleSet[8] = new Rule(2,2, "SoftBreak");
        RuleSet[9] = new Rule(3,0, "SoftBreak");
        RuleSet[10] = new Rule(3,1, "MediocreBreak");
        RuleSet[11] = new Rule(3,2, "MediocreBreak");
        RuleSet[12] = new Rule(4,0, "MediocreBreak");
        RuleSet[13] = new Rule(4,1, "MediocreBreak");
        RuleSet[14] = new Rule(4,2, "HardBreak");

        printRules(RuleSet);

        fuzzyPosition = fuzzify(positionInput, FuzzyPositionSet);
        fuzzySpeed = fuzzify(speedInput, FuzzySpeedSet);

        System.out.println("Memship Values of positionInput: " + positionInput + " = " + Arrays.toString(fuzzyPosition));
        System.out.println("Memship Values of speedInput: " + speedInput + " = " + Arrays.toString(fuzzySpeed));

        /* 3. Calculate Fuzzy Result*/
        fuzzyBreakResult = calculateFuzzyBreak(fuzzySpeed, fuzzyPosition, RuleSet);
        System.out.println(Arrays.toString(fuzzyBreakResult));
     //   System.out.println(RuleSet[14].isActive(testset, testset2)+""+RuleSet[14].getName());

        /* 4. DEFUZZIFY */
        System.out.println("Defuzzified Value of "+Arrays.toString(fuzzyBreakResult)+" =  "+defuzzify(fuzzyBreakResult, FuzzyBreakSet));
    }

     public static double[] fuzzify(double x, FuzzyFunction[] FuzzySet) {
        double[] fuzzyVal = new double[FuzzySet.length];
        for (int i = 0; i < FuzzySet.length; i++) {
            fuzzyVal[i] = FuzzySet[i].getMemshipValue(x);
        }
        return fuzzyVal;
    }

    public static double defuzzify(double[] defuzArray, FuzzyFunction[] FuzzySet) {
        double result = 0;
        double membershipSum = 0;
        for(int i = 0; i < defuzArray.length; i++) {
            if (defuzArray[i] > 0.0) {
                result += FuzzySet[i].getCentroid() * defuzArray[i];
                membershipSum += defuzArray[i];
            }
        }
        System.out.println("Result: "+result);
        System.out.println("Memshipsum: "+membershipSum);
        return result/membershipSum;
    }
    public static void printRules(Rule[] rules){

        System.out.println("\t\t\tSlow\t\tMediocre\t\tFast");
        System.out.println("-----------------------------------------------");
        for(int i = 0; i < 5; i++) {
            System.out.print(positionCategory[i]+"\t|");
            for(int j = 0; j < 3; j++) {
                System.out.print("\t"+rules[3*i+j].getName());
            }
            System.out.println();
        }
        System.out.println();
    }

    /*This Method checks the arrays fuzzySpeed and fuzzyPosition against the Ruleset and determines which
     rules are active.
     If a rule is active, it returns the minimum membership value of fuzzySpeed or fuzzyPosition.

     This function sets up a double Array which contains the maximum membership values of the minimum
     values determined by the rules.
      */


    public static double[] calculateFuzzyBreak(double[] fuzzySpeed, double[] fuzzyPosition, Rule[] rules) {
     double[] fuzzyBreak = new double[3];

     //Iterate through all rules
     for (int i = 0; i < rules.length; i++) {
         if(rules[i].isActive(fuzzySpeed, fuzzyPosition)) {
             String conclusion = rules[i].getName();
            // System.out.println(Arrays.toString(fuzzyBreak));
            //  System.out.println("Applied Rule: " + i + " Conclusion: " + conclusion);
             double tmp = rules[i].getMin(fuzzySpeed, fuzzyPosition);
            //  System.out.println("Current min value: "+tmp);


             switch (conclusion) {
                 case "SoftBreak":
                     if (tmp > fuzzyBreak[0]) {
                         fuzzyBreak[0] = tmp;
                     }
                     break;
                 case "MediocreBreak":
                     if (tmp > fuzzyBreak[1]) {
                         fuzzyBreak[1] = tmp;
                     }
                     break;
                 case "HardBreak":
                     if (tmp > fuzzyBreak[2]) {
                         fuzzyBreak[2] = tmp;
                     }
                     break;
             }
         }
     }
    return fuzzyBreak;
    }

}
