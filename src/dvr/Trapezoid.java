package dvr;

public class Trapezoid extends FuzzyFunction {
    private double dLeftMiddle, dRightMiddle;


    public Trapezoid(double dLeft, double leftPeak, double rightPeak, double dRight, String memshipName){
        this.dLeft = dLeft; // 0
        this.dLeftMiddle = leftPeak; // 0
        this.dRightMiddle = rightPeak; // 64
        this.dRight = dRight; // 192
        this.memshipName = memshipName;
        this.functionType = 't';
    }


    public double getMemshipValue(double x){
        if (x < dLeft || x > dRight) {return 0;}
        else if (x < dLeftMiddle) {
            return (x - dLeft )/(dLeftMiddle - dLeft); //Classic upper Linear Memship Function
        }
        else if (x >= dLeftMiddle && x <= dRightMiddle) {return 1.0;} // Peak Value
        else if (x < dRight) {
            return (dRight - x)/(dRight - dRightMiddle); //Classic downer Linear Memship Function
        }
        else return 0;
    }
    public double getCentroid() {
        double a = dRightMiddle - dLeftMiddle;
        double b = dRight - dLeft;
        double c = dLeftMiddle - dLeft;

        double result = (2*a*c + a*a + c*b + a*b+ b*b)/(3*(a+b));

        return result;
    }
}
