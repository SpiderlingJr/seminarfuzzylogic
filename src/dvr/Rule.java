package dvr;

import java.util.Arrays;

public class Rule {
    private int speedColumn;
    private int positionRow;
    private String conclusion;

    public Rule(int positionRow, int speedColumn, String conclusion){
    this.speedColumn = speedColumn;
    this.positionRow = positionRow;
    this.conclusion = conclusion;

    }
    public boolean isActive(double[] fuzzySpeed, double[] fuzzyPosition) {
     /*   System.out.println("pos: "+Arrays.toString(fuzzyPosition));
        System.out.println("speed: "+ Arrays.toString(fuzzySpeed));
        System.out.println("SpeedAccess: " + speedColumn + "\t Position Access: "+positionRow);*/
        if(fuzzySpeed[speedColumn] > 0 || fuzzyPosition[positionRow] > 0) {
            return true;
        } else return false;
    }
    public String getName() {
        return this.conclusion;
    }

    public double getMin(double[] fuzzySpeed, double[] fuzzyPosition) {
      /*  System.out.println("pos: "+Arrays.toString(fuzzyPosition));
        System.out.println("speed: "+ Arrays.toString(fuzzySpeed));
        System.out.println("SpeedAccess: " + speedColumn + "\t Position Access: "+positionRow);*/
        if(fuzzySpeed[speedColumn] < fuzzyPosition[positionRow]) {
        return fuzzySpeed[speedColumn];
    } else return fuzzyPosition[positionRow];
    }
}
